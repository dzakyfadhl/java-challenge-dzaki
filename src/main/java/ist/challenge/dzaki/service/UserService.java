package ist.challenge.dzaki.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import ist.challenge.dzaki.common.model.UserRequest;
import ist.challenge.dzaki.common.model.UserResponse;
import ist.challenge.dzaki.persistence.entity.User;
import ist.challenge.dzaki.persistence.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repo;

	public Optional<User> getEntityById(Long id) {
		return repo.findById(id);
	}

	public UserResponse login(UserRequest request) {
		if (request.getUsername() == null || request.getUsername().isBlank()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username Kosong");
		}

		if (request.getPassword() == null || request.getPassword().isBlank()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password Kosong");
		}

		User user = repo.findByUsernameAndPassword(request.getUsername(), request.getPassword());
		UserResponse response = new UserResponse();
		response.setId(user.getId());
		response.setUsername(user.getUsername());
		response.setPassword(user.getPassword());
		return response;
	}

	public List<UserResponse> getUsers() {
		List<User> users = repo.findAll();
		return users.stream().map(u -> {
			UserResponse response = new UserResponse();
			response.setId(u.getId());
			response.setUsername(u.getUsername());
			response.setPassword(u.getPassword());
			return response;
		}).toList();
	}

	public void add(UserRequest request) {
		validateUsernameExists(request.getUsername());

		User user = new User();
		user.setUsername(request.getUsername());
		user.setPassword(request.getPassword());
		repo.saveAndFlush(user);
	}

	public void edit(UserRequest request) {
		User user = getEntityById(request.getId())
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User tidak ditemukan"));

		if (user.getPassword().equals(request.getPassword())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Password tidak boleh sama dengan password sebelumnya");
		}

		user.setUsername(request.getUsername());
		user.setPassword(request.getPassword());
		repo.saveAndFlush(user);
	}

	public void validateUsernameExists(String username) {
		if (repo.existsByUsername(username))
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Username sudah terpakai");
	}

}
