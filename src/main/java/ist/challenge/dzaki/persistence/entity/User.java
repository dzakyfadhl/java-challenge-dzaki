package ist.challenge.dzaki.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(name = "tb_m_users", uniqueConstraints = { @UniqueConstraint(name = "bk_user", columnNames = { "username" }) })
public class User {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue
	private Long id;

	@Column(nullable = false, length = 25)
	private String username;

	@Column(nullable = false, length = 25)
	private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
