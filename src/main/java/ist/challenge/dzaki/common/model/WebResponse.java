package ist.challenge.dzaki.common.model;

public class WebResponse<T> {

	private Integer code;
	private T data;
	
	public WebResponse() {
	}
	
	public WebResponse(Integer code, T data) {
		super();
		this.code = code;
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
