package ist.challenge.dzaki.common.helper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ist.challenge.dzaki.common.model.WebResponse;

public class ResponseHelper {

	public static <T> ResponseEntity<WebResponse<T>> createWebResponse(T result, HttpStatus status) {
		WebResponse<T> webResponse = new WebResponse<>(status.value(), result);
		return new ResponseEntity<>(webResponse, status);
	}

}
