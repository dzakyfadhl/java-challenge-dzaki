package ist.challenge.dzaki.common.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import ist.challenge.dzaki.common.helper.ResponseHelper;
import ist.challenge.dzaki.common.model.WebResponse;

@RestControllerAdvice
public class CommonErrorController {

	@ExceptionHandler(value = { ResponseStatusException.class })
	public <E extends ResponseStatusException> ResponseEntity<WebResponse<String>> validationHandler2(E e) {
		e.printStackTrace();
		var httpStatus = HttpStatus.valueOf(e.getStatusCode().value());
		return ResponseHelper.createWebResponse(e.getReason(), httpStatus);
	}

}
