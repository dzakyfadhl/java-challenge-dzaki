package ist.challenge.dzaki.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.dzaki.common.helper.ResponseHelper;
import ist.challenge.dzaki.common.model.UserRequest;
import ist.challenge.dzaki.common.model.UserResponse;
import ist.challenge.dzaki.common.model.WebResponse;
import ist.challenge.dzaki.service.UserService;

@RestController
@RequestMapping({ "/users" })
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping
	public ResponseEntity<WebResponse<String>> add(@RequestBody UserRequest request) {
		service.add(request);
		return ResponseHelper.createWebResponse("Sukses", HttpStatus.CREATED);
	}

	@PostMapping(value = "/login")
	public ResponseEntity<WebResponse<UserResponse>> login(@RequestBody UserRequest request) {
		UserResponse response = service.login(request);
		return ResponseHelper.createWebResponse(response, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<WebResponse<String>> edit(@RequestBody UserRequest request) {
		service.edit(request);
		return ResponseHelper.createWebResponse("Sukses", HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<WebResponse<List<UserResponse>>> getUsers() {
		return ResponseHelper.createWebResponse(service.getUsers(), HttpStatus.OK);
	}

}
